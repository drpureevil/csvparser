﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsvParser
{
    class ConsoleView : IView
    {
        public ConsoleView(IParser parser)
        {
            this.parser = parser;
        }

        protected IParser parser;

        public void Display()
        {
            try
            {
                foreach (IList<string> row in parser.Parse())
                {
                    DisplayLine(row);
                }
            }
            catch (ParserException exc)
            {
                Console.WriteLine(string.Format("{0} on line {1}", exc.Message, exc.Line));
            }
        }
        protected void DisplayLine(IList<string> row)
        {
            StringBuilder sb = new StringBuilder("|");
            foreach (string field in row)
            {
                sb.Append(field);
                sb.Append("|");
            }
            Console.WriteLine(sb.ToString());
        }
    }
}

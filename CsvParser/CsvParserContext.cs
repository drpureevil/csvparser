﻿using System.Collections.Generic;
using System.Text;

namespace CsvParser
{
    class ParserContext
    {
        #region Members
        protected int line = 1;
        protected readonly StringBuilder sb = new StringBuilder();
        protected readonly List<string> fields = new List<string>();
        protected bool rowReady = false;
        #endregion
        #region Properties
        public int Line
        {
            get { return line; }
        }

        public bool RowReady
        {
            get { return rowReady; }
        }

        public IList<string> Row
        {
            get { return fields; }
        }
        #endregion
        #region Methods
        public void AddChar(char c)
        {
            sb.Append(c);
        }

        public void AddField(string s)
        {
            fields.Add(s);
        }
        public void AddField()
        {
            fields.Add(sb.ToString());
            sb.Clear();
        }

        public void AddRow()
        {
            if (sb.Length != 0 || fields.Count != 0)
            {
                line++;
                fields.Add(sb.ToString());
                sb.Clear();
                rowReady = true;
            }
        }

        public void ClearRow()
        {
            fields.Clear();
            rowReady = false;
        }
        #endregion
    }
}

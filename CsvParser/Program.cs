﻿using System;
using System.Text;
using System.IO;

namespace CsvParser
{
    sealed class Program
    {
        private void Usage()
        {
            Console.WriteLine("Usage: CsvParser path [-encoding e] [-column c [-value v]]");
        }
        private void ParseArgs(string[] args)
        {
            ArgumentParser ap = new ArgumentParser();

            if (args.Length == 0 || !ap.Parse(args))
            {
                Usage();
                return;
            }

            Run(args[0], (ap.Encoding == null ? Encoding.UTF8 : ap.Encoding), ap.Filter);
        }
        private void Run(string path, Encoding encoding, Tuple<int, string> filter)
        {
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(path, encoding);
            }
            catch (FileNotFoundException exc)
            {
                Console.WriteLine(string.Format("File \"{0}\" does not exist", exc.FileName));
                return;
            }

            IParser parser = new CsvParser(reader);
            if (filter != null)
                parser = new Filter(parser, filter.Item1, filter.Item2);
            IView view = new ConsoleView(parser);
            view.Display();
        }

        static void Main(string[] args)
        {
            Program app = new Program();
            app.ParseArgs(args);
        }
    }
}

﻿using System;
using System.Text;

namespace CsvParser
{
    class ArgumentParser
    {
        protected Encoding encoding = null;
        protected int filterColumn = -1;
        protected string filterValue = string.Empty;

        public Encoding Encoding
        {
            get { return encoding; }
        }

        public Tuple<int, string> Filter
        {
            get { return filterColumn == -1 ? null : Tuple.Create(filterColumn, filterValue); }
        }

        public bool Parse(string[] args)
        {
            for (int i = 1; i < args.Length; i++)
            {
                string option = args[i];
                if (++i >= args.Length)
                    return false;

                string value = args[i];
                switch (option)
                {
                    case "-encoding":
                        EncodingInfo info = Array.Find(Encoding.GetEncodings(), inf => inf.Name.ToUpper() == value.ToUpper());
                        if (info == null)
                            return false;
                        encoding = info.GetEncoding();
                        break;
                    case "-column":
                        if (!int.TryParse(value, out filterColumn))
                            return false;
                        break;
                    case "-value":
                        filterValue = value;
                        break;
                    default:
                        return false;
                }

            }
            return true;
        }
    }
}

﻿using System.Collections.Generic;

namespace CsvParser
{
    interface IParser
    {
        IEnumerable<IList<string>> Parse();
    }

    interface IView
    {
        void Display();
    }
}

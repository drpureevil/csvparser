﻿using System.Collections.Generic;

namespace CsvParser
{
    class Filter : IParser
    {
        public Filter(IParser parser, int column, string value)
        {
            this.parser = parser;
            this.column = column;
            this.value = value;
        }

        protected IParser parser;
        protected int column = 0;
        protected string value;
        public IEnumerable<IList<string>> Parse()
        {
            foreach(IList<string> row in parser.Parse())
            {
                if (column <= row.Count && row[column-1] == value)
                    yield return row;
            }
        }
    }
}

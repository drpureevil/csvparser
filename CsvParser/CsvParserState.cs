﻿namespace CsvParser
{
    abstract class ParserState
    {
        protected static readonly ParserState begFieldState = new BegFieldState();
        protected static readonly ParserState noQuotesState = new NoQuotesState();
        protected static readonly ParserState inQuotesState = new InQuotesState();
        protected static readonly ParserState quoteInQuotesState = new QuoteInQuotesState();
        protected static readonly ParserState carrierReturnState = new CarrierReturnState();

        protected const char comma = ',';
        protected const char quote = '"';
        protected const char cr = '\r';
        protected const char lf = '\n';

        public abstract ParserState ParseChar(ParserContext context, char c);
        public abstract void EndOfFile(ParserContext context);
    }

    class BegFieldState : ParserState
    {
        public override ParserState ParseChar(ParserContext context, char c)
        {
            switch (c)
            {
                case comma:
                    context.AddField(string.Empty);
                    return this;
                case quote:
                    return inQuotesState;
                case cr:
                    return carrierReturnState;
                case lf:
                    throw new ParserException("LF without preceding CR", context.Line);
                default:
                    context.AddChar(c);
                    return noQuotesState;
            }
        }

        public override void EndOfFile(ParserContext context)
        {
            context.AddRow();
        }
    }

    class NoQuotesState : ParserState
    {
        public override ParserState ParseChar(ParserContext context, char c)
        {
            switch (c)
            {
                case comma:
                    context.AddField();
                    return begFieldState;
                case cr:
                    return carrierReturnState;
                case lf:
                    throw new ParserException("LF without preceding CR", context.Line);
                case quote:
                    throw new ParserException("Quote within unquoted field", context.Line);
                default:
                    context.AddChar(c);
                    return this;
            }

        }

        public override void EndOfFile(ParserContext context)
        {
            context.AddRow();
        }
    }
    class InQuotesState : ParserState
    {
        public override ParserState ParseChar(ParserContext context, char c)
        {
            switch (c)
            {
                case quote:
                    return quoteInQuotesState;
                default:
                    context.AddChar(c);
                    return this;
            }

        }

        public override void EndOfFile(ParserContext context)
        {
            throw new ParserException("No closing quote", context.Line);
        }
    }
    class QuoteInQuotesState : ParserState
    {
        public override ParserState ParseChar(ParserContext context, char c)
        {
            switch (c)
            {
                case quote:
                    context.AddChar(quote);
                    return inQuotesState;
                case comma:
                    context.AddField();
                    return begFieldState;
                case cr:
                    return carrierReturnState;
                default:
                    throw new ParserException("Unexpected quote", context.Line);
            }
        }

        public override void EndOfFile(ParserContext context)
        {
            context.AddRow();
        }
    }
    class CarrierReturnState : ParserState
    {
        public override ParserState ParseChar(ParserContext context, char c)
        {
            switch (c)
            {
                case lf:
                    context.AddRow();
                    return begFieldState;
                default:
                    throw new ParserException("CR without following LF", context.Line);
            }
        }

        public override void EndOfFile(ParserContext context)
        {
            throw new ParserException("CR without following LF", context.Line);
        }
    }
}

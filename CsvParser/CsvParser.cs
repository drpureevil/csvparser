﻿using System.Collections.Generic;
using System.IO;

namespace CsvParser
{
    class CsvParser : IParser
    {
        public CsvParser(TextReader reader)
        {
            this.reader = reader;
        }

        protected readonly TextReader reader;

        public IEnumerable<IList<string>> Parse()
        {
            ParserContext context = new ParserContext();
            ParserState state = new BegFieldState();

            int result = 0;
            while ((result = reader.Read()) != -1)
            {
                char c = (char)result;
                state = state.ParseChar(context, c);
                if (context.RowReady)
                {
                    yield return context.Row;
                    context.ClearRow();
                }
            }
            state.EndOfFile(context);
            if (context.RowReady)
                yield return context.Row;
        }
    }
}
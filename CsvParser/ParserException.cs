﻿using System;

namespace CsvParser
{
    class ParserException : Exception
    {
        public ParserException(string message, int line) : base(message)
        {
            this.line = line;
        }

        protected readonly int line;

        public int Line
        {
            get { return line; }
        }
    }
}
